package facci.joanreyes.examen2doparcial.Adaptador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import facci.joanreyes.examen2doparcial.Modelo.Estudiantes;
import facci.joanreyes.examen2doparcial.R;



public class AdaptadorEstudiantes extends RecyclerView.Adapter<AdaptadorEstudiantes.ViewHolder>{

    ArrayList<Estudiantes> estudiantesArrayList;

    public AdaptadorEstudiantes(ArrayList<Estudiantes> estudiantesArrayList) {
        this.estudiantesArrayList = estudiantesArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.estudiante, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Estudiantes estudiantes = estudiantesArrayList.get(i);
        viewHolder.nombres.setText("NOMBRES: " + estudiantes.getNombres());
        viewHolder.apellidos.setText("APELLIDOS: " + estudiantes.getApellidos());
        viewHolder.parcial1.setText("PARCIAL UNO: " + estudiantes.getParcial_uno());
        viewHolder.parcial2.setText("PARCIAL DOS: " + estudiantes.getParcial_uno());
        Picasso.get().load(estudiantes.getImagen()).into(viewHolder.foto);

    }

    @Override
    public int getItemCount() {
        return estudiantesArrayList.size();
    }




    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView nombres, apellidos, parcial1, parcial2;
        ImageView foto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nombres = (TextView)itemView.findViewById(R.id.LBLNombresE);
            apellidos = (TextView)itemView.findViewById(R.id.LBLApellidosE);
            parcial1 = (TextView)itemView.findViewById(R.id.LBLParcial1E);
            parcial2 = (TextView)itemView.findViewById(R.id.LBLPArcial2E);
            foto = (ImageView)itemView.findViewById(R.id.ImagenEstudiante);
        }
    }
}